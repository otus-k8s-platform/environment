# environment: logs, monitoring, ingress

kubectl create clusterrolebinding cluster-admin-binding     --clusterrole=cluster-admin     --user=$(gcloud config get-value core/account)

Install Ingress
cd ingress
helm upgrade --install --namespace environment --create-namespace ingress-nginx ingress-nginx/ingress-nginx --version v2.15.0 -f custom-values.yaml
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission

Install Certmanager

helm upgrade --install --namespace cert-manager --create-namespace cert-manager jetstack/cert-manager --version v1.0.2 --set installCRDs=true
cd certmanager
kubectl apply -f gencert.yaml

Install Prom
cd monitoring
helm upgrade --install --namespace environment --create-namespace prometheus stable/prometheus-operator --version v8.15.10 -f custom-values.yaml

Install EFK
cd efk
helm upgrade --install --namespace environment --create-namespace efk-stack stable/elastic-stack -f custom-values.yaml
